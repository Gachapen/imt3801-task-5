#ifndef THREADSAFEQUEUE_H_
#define THREADSAFEQUEUE_H_

#include <deque>
#include <condition_variable>
#include <omp.h>

template<class Type>
class ThreadSafeQueue {
public:
	ThreadSafeQueue();

	void push(const Type& value);
	Type pop();
	void clear();
	unsigned int size();
	bool isEmpty();
	void waitForItem();

private:
	std::deque<Type> queue;

	omp_lock_t pushLock;
	omp_lock_t popLock;
	std::condition_variable freshItemCondition;
	std::mutex freshItemMutex;
};

template<class Type>
ThreadSafeQueue<Type>::ThreadSafeQueue()
{
	omp_init_lock(&this->pushLock);
	omp_init_lock(&this->popLock);
}

template<class Type>
void ThreadSafeQueue<Type>::waitForItem()
{
	std::unique_lock<std::mutex> lock(this->freshItemMutex);

	if (this->isEmpty() == true) {
		this->freshItemCondition.wait(lock, [this]{ return this->queue.empty() == false; });
	}
}

template<class Type>
void ThreadSafeQueue<Type>::push(const Type& value)
{
	omp_set_lock(&this->pushLock);

	this->queue.push_back(value);

	omp_unset_lock(&this->pushLock);

	freshItemCondition.notify_one();
}

template<class Type>
Type ThreadSafeQueue<Type>::pop()
{
	omp_set_lock(&this->popLock);

	Type value;

	if (this->queue.empty() == true) {
		value = Type();
	} else {
		value = this->queue.front();
		this->queue.pop_front();
	}

	omp_unset_lock(&this->popLock);

	return value;
}

template<class Type>
unsigned int ThreadSafeQueue<Type>::size()
{
	omp_set_lock(&this->popLock);
	omp_set_lock(&this->pushLock);

	unsigned int size = this->queue.size();

	omp_unset_lock(&this->pushLock);
	omp_unset_lock(&this->popLock);
	
	return size;
}

template<class Type>
bool ThreadSafeQueue<Type>::isEmpty() 
{
	bool empty;

	omp_set_lock(&this->popLock);
	omp_set_lock(&this->pushLock);
	
	empty = this->queue.empty();
	
	omp_unset_lock(&this->pushLock);
	omp_unset_lock(&this->popLock);

	return empty;
}

template<class Type>
void ThreadSafeQueue<Type>::clear()
{
	omp_set_lock(&this->popLock);
	omp_set_lock(&this->pushLock);
	
	this->queue.clear();
	
	omp_unset_lock(&this->pushLock);
	omp_unset_lock(&this->popLock);
}

#endif
