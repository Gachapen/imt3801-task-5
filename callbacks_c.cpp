#include "callbacks_c.h"

bool useAlternativeMapping = 0;

void toggleKeyMapping()
{
	useAlternativeMapping = !useAlternativeMapping;
}

CallbackFunction getCallback(int key)
{
	switch (key)
	{
	case 1:
		if (useAlternativeMapping == 0) {
			return &getKey1;
		} else {
			return &getKey5;
		}
		break;
	case 2:
		if (useAlternativeMapping == 0) {
			return &getKey2;
		} else {
			return &getKey3;
		}
		break;
	case 3:
		if (useAlternativeMapping == 0) {
			return &getKey3;
		} else {
			return &getKey1;
		}
		break;
	case 4:
		if (useAlternativeMapping == 0) {
			return &getKey4;
		} else {
			return &getKey2;
		}
		break;
	case 5:
		if (useAlternativeMapping == 0) {
			return &getKey5;
		} else {
			return &getKey4;
		}
		break;
	}
}

int getKey1()
{
	return 1;
}

int getKey2()
{
	return 2;
}

int getKey3()
{
	return 3;
}

int getKey4()
{
	return 4;
}

int getKey5()
{
	return 5;
}
