#include "Callbacks.h"

Callbacks::Callbacks()
{
	for (size_t i = 1; i <= 5; i++) {
		this->callbackMappings[i].useAlternative = false;
	}

	this->callbackMappings[1].originalMapping = &Callbacks::getKey1;
	this->callbackMappings[1].alternativeMapping = &Callbacks::getKey5;
	this->callbackMappings[2].originalMapping = &Callbacks::getKey2;
	this->callbackMappings[2].alternativeMapping = &Callbacks::getKey3;
	this->callbackMappings[3].originalMapping = &Callbacks::getKey3;
	this->callbackMappings[3].alternativeMapping = &Callbacks::getKey1;
	this->callbackMappings[4].originalMapping = &Callbacks::getKey4;
	this->callbackMappings[4].alternativeMapping = &Callbacks::getKey2;
	this->callbackMappings[5].originalMapping = &Callbacks::getKey5;
	this->callbackMappings[5].alternativeMapping = &Callbacks::getKey4;
}

int Callbacks::getKey1()
{
	return 1;
}

int Callbacks::getKey2()
{
	return 2;
}

int Callbacks::getKey3()
{
	return 3;
}

int Callbacks::getKey4()
{
	return 4;
}

int Callbacks::getKey5()
{
	return 5;
}

CallbackFunction Callbacks::getCallback(int key)
{
	CallbackMapping& mapping = callbackMappings[key];
	CallbackFunction callback;

	if (mapping.useAlternative == false) {
		callback = mapping.originalMapping;
	} else {
		callback = mapping.alternativeMapping;
	}
	
	mapping.useAlternative = !mapping.useAlternative;
	
	return callback;
}
