typedef int (*CallbackFunction)();

CallbackFunction getCallback(int key);
void toggleKeyMapping();

int getKey1();
int getKey2();
int getKey3();
int getKey4();
int getKey5();
