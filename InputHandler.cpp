#include "InputHandler.h"

#include <iostream>

void InputHandler::run(ThreadSafeQueue<int>& processQueue)
{
	this->toggleKeyMapping();

	std::string input;
	std::getline(std::cin, input, '\n');

	while (input != "q")
	{
		if (input == " ") {
			this->toggleKeyMapping();
		} else {
			try {
				int inputNumber = std::stoi(input);
				if (inputNumber != 0) {
					inputNumber = this->mapKey(inputNumber);
					printf("Input: %i\n", inputNumber);
					processQueue.push(inputNumber);
				}
			} catch (std::invalid_argument& exception) {
				printf("Input not a number.\n");
			} catch (std::out_of_range& exception) {
				printf("Input too large.\n");
			}
		}

		std::getline(std::cin, input, '\n');
	}

	processQueue.push(0);
}

void InputHandler::toggleKeyMapping()
{
	for (int key = 1; key <= 5; key++) {
		this->keyMap[key] = callbacks.getCallback(key);
	}
}

int InputHandler::mapKey(int key)
{
	auto keyMapIt = this->keyMap.find(key);
	if (keyMapIt != this->keyMap.end()) {
		CallbackFunction callback = keyMapIt->second;
		return (this->callbacks.*callback)();
	} else {
		return key;
	}
}
