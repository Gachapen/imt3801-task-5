#ifndef CALLBACKS_H_
#define CALLBACKS_H_

#include <unordered_map>

class Callbacks;

typedef int (Callbacks::*CallbackFunction)();

class Callbacks {
public:
	Callbacks();

	CallbackFunction getCallback(int key);

	int getKey1();
	int getKey2();
	int getKey3();
	int getKey4();
	int getKey5();

private:
	struct CallbackMapping
	{
		CallbackFunction originalMapping;
		CallbackFunction alternativeMapping;
		bool useAlternative;
	};

	std::unordered_map<int, CallbackMapping> callbackMappings;
};

#endif
