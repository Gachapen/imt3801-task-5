#ifndef INPUTHANDLER_H_
#define INPUTHANDLER_H_

#include <unordered_map>
#include "ThreadSafeQueue.h"
#include "Callbacks.h"

class InputHandler {
public:
	void run(ThreadSafeQueue<int>& processQueue);

private:
	void toggleKeyMapping();
	int mapKey(int key);

	Callbacks callbacks;
	std::unordered_map<int, CallbackFunction> keyMap;
};

#endif
