#include "OutputPrinter.h"

#include <iostream>

void OutputPrinter::run()
{
	bool quit = false;
	
	while (quit == false) {
		this->inputQueue.waitForItem();

		while (this->inputQueue.isEmpty() == false) {
			int number = this->inputQueue.pop();
	
			if (number == 0) {
				quit = true;
			} else {
				printf("Output: %i\n", number);
			}
		}
	}
}

ThreadSafeQueue<int>& OutputPrinter::getQueue()
{
	return this->inputQueue;
}
