#include "SquareCalculator.h"

#include <iostream>

void SquareCalculator::run(ThreadSafeQueue<int>& outputQueue)
{
	bool quit = false;
	
	while (quit == false) {
		this->inputQueue.waitForItem();

		while (this->inputQueue.isEmpty() == false) {
			int number = this->inputQueue.pop();
			
			if (number == 0) {
				quit = true;
				outputQueue.push(0);
			} else {
				// Special case for negative numbers. Convert them to positive.
				if (number < 0) {
					number *= -1;
				}

				int squaredNumber = 0;
	
				#pragma omp parallel for reduction (+:squaredNumber)
				for (int i = 0; i < number; i++) {
					squaredNumber += number;
				}
		
				printf("Squared: %i\n", squaredNumber);
				outputQueue.push(squaredNumber);
			}
		}
	}
}

ThreadSafeQueue<int>& SquareCalculator::getQueue()
{
	return this->inputQueue;
}
