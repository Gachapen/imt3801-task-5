CC = g++
CFLAGS = -Wall -std=c++11 -pthread
LDFLAGS = -pthread -fopenmp

SRC = task_1.cpp InputHandler.cpp SquareCalculator.cpp OutputPrinter.cpp
OBJ = ${SRC:.cpp=.o}

.cpp.o:
	$(CC) $(CFLAGS) -c $<

Callbacks.o: Callbacks.h

callbacks_c.o: callbacks_c.h

InputHandler.o: InputHandler.h ThreadSafeQueue.h

SquareCalculator.o: SquareCalculator.h ThreadSafeQueue.h

OutputPrinter.o: OutputPrinter.h ThreadSafeQueue.h

task_1.o: InputHandler.h SquareCalculator.h OutputPrinter.h ThreadSafeQueue.h Callbacks.h

task_2.o: callbacks_c.h

task_1: InputHandler.o SquareCalculator.o OutputPrinter.o Callbacks.o task_1.o
	$(CC) $(LDFLAGS) -o $@ $^

task_2: task_2.o callbacks_c.o
	$(CC) $(LDFLAGS) -o $@ $^

all: task_1 task_2

clean:
	rm -f task_1 task_2 *.o
	
